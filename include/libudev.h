/*
 * Copyright (C) Patrick Steinhardt, 2020
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef LIBUDEV_H
#define LIBUDEV_H

#include <sys/sysmacros.h>
#include <sys/types.h>

struct udev;
struct udev_device;
struct udev_enumerate;
struct udev_monitor;
struct udev_list_entry;

struct udev *udev_new(void);
struct udev *udev_ref(struct udev *udev);
struct udev *udev_unref(struct udev *udev);

struct udev_device *udev_device_new_from_devnum(struct udev *udev, char type, dev_t devnum);
struct udev_device *udev_device_new_from_syspath(struct udev *udev, const char *syspath);
struct udev_device *udev_device_new_from_nulstr(struct udev *udev, char *buf, ssize_t buflen);
struct udev_device *udev_device_ref(struct udev_device *dev);
struct udev_device *udev_device_unref(struct udev_device *dev);
struct udev *udev_device_get_udev(struct udev_device *dev);
const char *udev_device_get_action(struct udev_device *dev);
const char *udev_device_get_devnode(struct udev_device *dev);
const char *udev_device_get_devtype(struct udev_device *dev);
dev_t udev_device_get_devnum(struct udev_device *dev);
int udev_device_get_is_initialized(struct udev_device *dev);
struct udev_device *udev_device_get_parent(struct udev_device *dev);
struct udev_device *udev_device_get_parent_with_subsystem_devtype(struct udev_device *dev, const char *subsystem, const char *devtype);
struct udev_list_entry *udev_device_get_properties_list_entry(struct udev_device *dev);
const char *udev_device_get_property_value(struct udev_device *dev, const char *property);
const char *udev_device_get_subsystem(struct udev_device *dev);
const char *udev_device_get_sysname(struct udev_device *dev);
const char *udev_device_get_syspath(struct udev_device *dev);
const char *udev_device_get_sysattr_value(struct udev_device *dev, const char *sysattr);

struct udev_enumerate *udev_enumerate_new(struct udev *udev);
struct udev_enumerate *udev_enumerate_unref(struct udev_enumerate *enumerate);
int udev_enumerate_add_match_subsystem(struct udev_enumerate *enumerate, const char *subsystem);
int udev_enumerate_add_match_sysname(struct udev_enumerate *enumerate, const char *sysname);
int udev_enumerate_scan_devices(struct udev_enumerate *enumerate);
struct udev_list_entry *udev_enumerate_get_list_entry(struct udev_enumerate *enumerate);

struct udev_monitor *udev_monitor_new_from_netlink(struct udev *udev, const char *name);
struct udev_monitor *udev_monitor_unref(struct udev_monitor *monitor);
int udev_monitor_enable_receiving(struct udev_monitor *monitor);
int udev_monitor_filter_add_match_subsystem_devtype(struct udev_monitor *monitor, const char *subsystem, const char *devtype);
struct udev *udev_monitor_get_udev(struct udev_monitor *monitor);
int udev_monitor_get_fd(struct udev_monitor *monitor);
struct udev_device *udev_monitor_receive_device(struct udev_monitor *monitor);

const char *udev_list_entry_get_name(struct udev_list_entry *e);
const char *udev_list_entry_get_value(struct udev_list_entry *e);
struct udev_list_entry *udev_list_entry_get_next(struct udev_list_entry *e);
struct udev_list_entry *udev_list_entry_get_by_name(struct udev_list_entry *e, const char *name);
#define udev_list_entry_foreach(entry, list) \
	for (entry = list; entry; entry = udev_list_entry_get_next(entry))

#endif /* LIBUDEV_H */
