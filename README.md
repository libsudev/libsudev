libsudev
========

libsudev is a pure C implemntation of the libudev application programming
interface. It's main intent is to be API compatible with libudev while not
requiring any kind of daemon and thus act as a compatibility wrapper for
packages that have a build dependency on libudev.
