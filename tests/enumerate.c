/*
 * Copyright (C) Patrick Steinhardt, 2020
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "common.h"

static struct udev *udev;

static int setup(void **state UNUSED)
{
	setenv("UDEV_SYSPATH_PREFIX", test_pathf("sys-simple", ""), 1);
	udev = udev_new();
	return 0;
}

static int teardown(void **state UNUSED)
{
	udev_unref(udev);
	return 0;
}

static void assert_devices(struct udev_list_entry *list, const char *device, ...)
{
	va_list ap;

	va_start(ap, device);
	while (device) {
		assert_non_null(list);
		assert_string_equal(udev_list_entry_get_name(list), test_pathf("sys-simple", device));
		assert_null(udev_list_entry_get_value(list));
		list = udev_list_entry_get_next(list);
		device = va_arg(ap, const char *);
	}
	va_end(ap);

	assert_null(list);
}

static void udev_enumerate_without_filters_succeeds(void **state UNUSED)
{
	struct udev_enumerate *enumerate;
	struct udev_list_entry *devices;

	assert_non_null(enumerate = udev_enumerate_new(udev));
	assert_return_code(udev_enumerate_scan_devices(enumerate), 0);
	assert_non_null(devices = udev_enumerate_get_list_entry(enumerate));
	assert_devices(devices, "/bus/pci/devices/0000:00:13.0", "/bus/scsi/devices/2:0:0:0", "/class/block/sda", "/class/mem/null", NULL);

	udev_enumerate_unref(enumerate);
}

static void udev_enumerate_with_subsystem_filter_succeeds(void **state UNUSED)
{
	struct udev_enumerate *enumerate;
	struct udev_list_entry *devices;

	assert_non_null(enumerate = udev_enumerate_new(udev));
	assert_return_code(udev_enumerate_add_match_subsystem(enumerate, "block"), 0);
	assert_return_code(udev_enumerate_scan_devices(enumerate), 0);
	assert_non_null(devices = udev_enumerate_get_list_entry(enumerate));
	assert_devices(devices, "/class/block/sda", NULL);

	udev_enumerate_unref(enumerate);
}

static void udev_enumerate_with_sysname_filter_succeeds(void **state UNUSED)
{
	struct udev_enumerate *enumerate;
	struct udev_list_entry *devices;

	assert_non_null(enumerate = udev_enumerate_new(udev));
	assert_return_code(udev_enumerate_add_match_sysname(enumerate, "sda"), 0);
	assert_return_code(udev_enumerate_scan_devices(enumerate), 0);
	assert_non_null(devices = udev_enumerate_get_list_entry(enumerate));
	assert_devices(devices, "/class/block/sda", NULL);

	udev_enumerate_unref(enumerate);
}

static void udev_enumerate_with_sysname_pattern_filter_succeeds(void **state UNUSED)
{
	struct udev_enumerate *enumerate;
	struct udev_list_entry *devices;

	assert_non_null(enumerate = udev_enumerate_new(udev));
	assert_return_code(udev_enumerate_add_match_sysname(enumerate, "sd*"), 0);
	assert_return_code(udev_enumerate_scan_devices(enumerate), 0);
	assert_non_null(devices = udev_enumerate_get_list_entry(enumerate));
	assert_devices(devices, "/class/block/sda", NULL);

	udev_enumerate_unref(enumerate);
}

int main(int argc, char *argv[])
{
	const struct CMUnitTest tests[] = {
		cmocka_unit_test(udev_enumerate_without_filters_succeeds),
		cmocka_unit_test(udev_enumerate_with_subsystem_filter_succeeds),
		cmocka_unit_test(udev_enumerate_with_sysname_filter_succeeds),
		cmocka_unit_test(udev_enumerate_with_sysname_pattern_filter_succeeds),
	};

	if ((test_parse_opts(argc, argv)) < 0)
		return -1;

	return cmocka_run_group_tests_name("enumerate", tests, setup, teardown);
}
