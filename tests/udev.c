/*
 * Copyright (C) Patrick Steinhardt, 2020
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "common.h"

static void udev_new_succeeds(void **state UNUSED)
{
	struct udev *udev;
	assert_non_null(udev = udev_new());
	assert_null(udev_unref(udev));
}

static void udev_ref_increases_refcount(void **state UNUSED)
{
	struct udev *udev;
	assert_non_null(udev = udev_new());
	assert_non_null(udev = udev_ref(udev));
	assert_null(udev_unref(udev));
	assert_null(udev_unref(udev));
}

static void udev_ref_on_null_pointer_succeeds(void **state UNUSED)
{
	assert_null(udev_ref(NULL));
}

static void udev_unref_on_null_pointer_succeeds(void **state UNUSED)
{
	assert_null(udev_unref(NULL));
}

int main(int argc, char *argv[])
{
	const struct CMUnitTest tests[] = {
		cmocka_unit_test(udev_new_succeeds),
		cmocka_unit_test(udev_ref_increases_refcount),
		cmocka_unit_test(udev_ref_on_null_pointer_succeeds),
		cmocka_unit_test(udev_unref_on_null_pointer_succeeds),
	};

	if ((test_parse_opts(argc, argv)) < 0)
		return -1;

	return cmocka_run_group_tests_name("udev", tests, NULL, NULL);
}
