/*
 * Copyright (C) Patrick Steinhardt, 2020
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "common.h"

#include <unistd.h>

int test_parse_opts(int argc, char *argv[])
{
	int error = 0;
	char c;

	while ((c = getopt(argc, argv, "o:s:h")) != -1) {
		switch (c) {
			case 'o':
				cmocka_set_test_filter(optarg);
				break;
			case 's':
				cmocka_set_skip_filter(optarg);
				break;
			case 'h':
			case ':':
			case '?':
				error = 1;
				break;
		}
	}

	if (optind < argc) {
		fprintf(stderr, "unexpected arguments\n");
		error = 1;
	}

	if (error) {
		fprintf(stderr, "usage: %s [-o <PATTERN>] [-s <PATTERN>] [-h]\n", argv[0]);
		fprintf(stderr, "       -o <PATTERN>: Execute only tests matching pattern\n");
		fprintf(stderr, "       -s <PATTERN>: Skip tests matching pattern\n");
		fprintf(stderr, "       -h:           Print this help\n");
		return -1;
	}

	return 0;
}

char *test_pathf(const char *fixture, const char *fmt, ...)
{
	static char path[PATH_MAX];
	va_list ap;
	int len;

	if ((len = snprintf(path, sizeof(path), "%s/%s", UDEV_SYSPATH_TEST_PREFIX, fixture)) < 0)
		return NULL;

	va_start(ap, fmt);
	if ((len = vsnprintf(path + len, sizeof(path) - len, fmt, ap)) < 0)
		return NULL;
	va_end(ap);

	return path;
}
