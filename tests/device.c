/*
 * Copyright (C) Patrick Steinhardt, 2020
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "common.h"

static struct udev *udev;

static int setup(void **state UNUSED)
{
	setenv("UDEV_SYSPATH_PREFIX", test_pathf("sys-simple", ""), 1);
	udev = udev_new();
	return 0;
}

static int teardown(void **state UNUSED)
{
	udev_unref(udev);
	return 0;
}

static void udev_device_from_syspath_existing_succeeds(void **state UNUSED)
{
	struct udev_device *dev;
	assert_non_null(dev = udev_device_new_from_syspath(udev, test_pathf("sys-simple", "/class/block/sda")));
	assert_null(udev_device_unref(dev));
}

static void udev_device_from_syspath_nonexisting_fails(void **state UNUSED)
{
	assert_null(udev_device_new_from_syspath(udev, test_pathf("sys-simple", "/class/block/sdx")));
}

static void udev_device_from_nulstr_succeeds(void **state UNUSED)
{
	char string[] = "FOO=bar\0DEVPATH=/class/block/sda";
	struct udev_device *dev;
	assert_non_null(dev = udev_device_new_from_nulstr(udev, string, sizeof(string)));
	assert_null(udev_device_unref(dev));
}

static void udev_device_from_devnum_existing_succeeds(void **state UNUSED)
{
	struct udev_device *dev;
	assert_non_null(dev = udev_device_new_from_devnum(udev, 'b', makedev(8, 0)));
	assert_null(udev_device_unref(dev));
}

static void udev_device_from_devnum_nonexisting_fails(void **state UNUSED)
{
	assert_null(udev_device_new_from_devnum(udev, 'b', makedev(17, 3)));
}

static void udev_device_from_devnum_with_wrong_type_fails(void **state UNUSED)
{
	assert_null(udev_device_new_from_devnum(udev, 'c', makedev(8, 0)));
}

static void udev_device_from_devnum_with_invalid_type_fails(void **state UNUSED)
{
	assert_null(udev_device_new_from_devnum(udev, 'x', makedev(8, 0)));
}

static void udev_device_get_parent_succeeds(void **state UNUSED)
{
	struct udev_device *dev, *parent;

	assert_non_null(dev = udev_device_new_from_syspath(udev, test_pathf("sys-simple", "/class/block/sda")));
	assert_non_null(parent = udev_device_get_parent(dev));
	assert_string_equal(udev_device_get_syspath(parent), test_pathf("sys-simple", "/devices/pci0000:00/0000:00:13.0/ata1/host2/target2:0:0/2:0:0:0"));

	assert_null(udev_device_unref(dev));
	assert_null(udev_device_unref(parent));
}

static void udev_device_get_parent_fails_at_root(void **state UNUSED)
{
	const char *parent_paths[] = {
		"/devices/pci0000:00/0000:00:13.0/ata1/host2/target2:0:0/2:0:0:0",
		"/devices/pci0000:00/0000:00:13.0/ata1/host2/target2:0:0",
		"/devices/pci0000:00/0000:00:13.0/ata1/host2",
		"/devices/pci0000:00/0000:00:13.0/ata1",
		"/devices/pci0000:00/0000:00:13.0",
		"/devices/pci0000:00",
		NULL,
	};
	struct udev_device *dev;
	size_t i;

	assert_non_null(dev = udev_device_new_from_syspath(udev, test_pathf("sys-simple", "/class/block/sda")));

	for (i = 0; i < ARRAY_SIZE(parent_paths); i++) {
		struct udev_device *parent = udev_device_get_parent(dev);

		if (!parent_paths[i]) {
			assert_null(parent);
			break;
		}

		assert_non_null(parent);
		assert_string_equal(udev_device_get_syspath(parent), test_pathf("sys-simple", parent_paths[i]));

		assert_null(udev_device_unref(dev));
		dev = parent;
	}

	assert_null(udev_device_unref(dev));
}

static void udev_device_get_parent_with_subsystem_devtype_succeeds(void **state UNUSED)
{
	struct udev_device *dev, *parent;
	assert_non_null(dev = udev_device_new_from_syspath(udev, test_pathf("sys-simple", "/class/block/sda")));
	assert_non_null(parent = udev_device_get_parent_with_subsystem_devtype(dev, "scsi", "scsi_host"));
	assert_string_equal(udev_device_get_syspath(parent), test_pathf("sys-simple", "/devices/pci0000:00/0000:00:13.0/ata1/host2"));
	assert_null(udev_device_unref(dev));
	assert_null(udev_device_unref(parent));
}

static void udev_device_get_parent_with_subsystem_devtype_succeeds_with_missing_devtype(void **state UNUSED)
{
	struct udev_device *dev, *parent;
	assert_non_null(dev = udev_device_new_from_syspath(udev, test_pathf("sys-simple", "/class/block/sda")));
	assert_non_null(parent = udev_device_get_parent_with_subsystem_devtype(dev, "pci", NULL));
	assert_string_equal(udev_device_get_syspath(parent), test_pathf("sys-simple", "/devices/pci0000:00/0000:00:13.0"));
	assert_null(udev_device_unref(dev));
	assert_null(udev_device_unref(parent));
}

static void udev_device_get_parent_with_subsystem_devtype_fails_with_non_matching_devtype(void **state UNUSED)
{
	struct udev_device *dev;
	assert_non_null(dev = udev_device_new_from_syspath(udev, test_pathf("sys-simple", "/class/block/sda")));
	assert_null(udev_device_get_parent_with_subsystem_devtype(dev, "foobar", NULL));
	assert_null(udev_device_unref(dev));
}

static void udev_device_get_subsystem_succeeds(void **state UNUSED)
{
	struct udev_device *dev;
	assert_non_null(dev = udev_device_new_from_syspath(udev, test_pathf("sys-simple", "/class/block/sda")));
	assert_string_equal(udev_device_get_subsystem(dev), "block");
	assert_null(udev_device_unref(dev));
}

static void udev_device_get_syspath_succeeds(void **state UNUSED)
{
	struct udev_device *dev;
	assert_non_null(dev = udev_device_new_from_syspath(udev, test_pathf("sys-simple", "/class/block/sda")));
	assert_string_equal(udev_device_get_syspath(dev), test_pathf("sys-simple", "/devices/pci0000:00/0000:00:13.0/ata1/host2/target2:0:0/2:0:0:0/block/sda"));
	assert_null(udev_device_unref(dev));
}

static void udev_device_get_sysname_succeeds(void **state UNUSED)
{
	struct udev_device *dev;
	assert_non_null(dev = udev_device_new_from_syspath(udev, test_pathf("sys-simple", "/class/block/sda")));
	assert_string_equal(udev_device_get_sysname(dev), "sda");
	assert_null(udev_device_unref(dev));
}

static void udev_device_get_devnode_succeeds(void **state UNUSED)
{
	struct udev_device *dev;
	assert_non_null(dev = udev_device_new_from_syspath(udev, test_pathf("sys-simple", "/class/block/sda")));
	assert_string_equal(udev_device_get_devnode(dev), "/dev/sda");
	assert_null(udev_device_unref(dev));
}

static void udev_device_get_devtype_succeeds(void **state UNUSED)
{
	struct udev_device *dev;
	assert_non_null(dev = udev_device_new_from_syspath(udev, test_pathf("sys-simple", "/class/block/sda")));
	assert_string_equal(udev_device_get_devtype(dev), "disk");
	assert_null(udev_device_unref(dev));
}

static void udev_device_get_devnum_succeeds(void **state UNUSED)
{
	struct udev_device *dev;
	dev_t devnum;

	assert_non_null(dev = udev_device_new_from_syspath(udev, test_pathf("sys-simple", "/class/block/sda")));
	devnum = udev_device_get_devnum(dev);
	assert_int_equal(major(devnum), 8);
	assert_int_equal(minor(devnum), 0);

	assert_null(udev_device_unref(dev));
}

static void udev_device_get_property_value_succeeds(void **state UNUSED)
{
	struct udev_device *dev;
	assert_non_null(dev = udev_device_new_from_syspath(udev, test_pathf("sys-simple", "/class/block/sda")));
	assert_string_equal(udev_device_get_property_value(dev, "DEVNAME"), "sda");
	assert_string_equal(udev_device_get_property_value(dev, "MAJOR"), "8");
	assert_null(udev_device_unref(dev));
}

static void udev_device_get_property_value_returns_null_for_nonexisting_property(void **state UNUSED)
{
	struct udev_device *dev;
	assert_non_null(dev = udev_device_new_from_syspath(udev, test_pathf("sys-simple", "/class/block/sda")));
	assert_null(udev_device_get_property_value(dev, "FOOBAR"));
	assert_null(udev_device_unref(dev));
}

static void udev_device_get_property_value_returns_null_for_prefix(void **state UNUSED)
{
	struct udev_device *dev;
	assert_non_null(dev = udev_device_new_from_syspath(udev, test_pathf("sys-simple", "/class/block/sda")));
	assert_null(udev_device_get_property_value(dev, "DEVNAM"));
	assert_null(udev_device_unref(dev));
}

static void udev_device_get_sysattr_value_succeeds(void **state UNUSED)
{
	struct udev_device *dev;
	assert_non_null(dev = udev_device_new_from_syspath(udev, test_pathf("sys-simple", "/class/block/sda")));
	assert_string_equal(udev_device_get_sysattr_value(dev, "removable"), "0\n");
	assert_null(udev_device_unref(dev));
}

static void udev_device_get_sysattr_value_succeeds_with_multiple_values(void **state UNUSED)
{
	struct udev_device *dev;
	assert_non_null(dev = udev_device_new_from_syspath(udev, test_pathf("sys-simple", "/class/block/sda")));
	assert_string_equal(udev_device_get_sysattr_value(dev, "removable"), "0\n");
	assert_string_equal(udev_device_get_sysattr_value(dev, "size"), "9763248\n");
	assert_null(udev_device_unref(dev));
}

static void udev_device_get_sysattr_value_succeeds_with_cached_value(void **state UNUSED)
{
	struct udev_device *dev;
	assert_non_null(dev = udev_device_new_from_syspath(udev, test_pathf("sys-simple", "/class/block/sda")));
	assert_string_equal(udev_device_get_sysattr_value(dev, "removable"), "0\n");
	assert_string_equal(udev_device_get_sysattr_value(dev, "removable"), "0\n");
	assert_null(udev_device_unref(dev));
}

static void udev_device_get_sysattr_value_fails_with_nonexisting_sysvalue(void **state UNUSED)
{
	struct udev_device *dev;
	assert_non_null(dev = udev_device_new_from_syspath(udev, test_pathf("sys-simple", "/class/block/sda")));
	assert_null(udev_device_get_sysattr_value(dev, "foobar"));
	assert_null(udev_device_unref(dev));
}

int main(int argc, char *argv[])
{
	const struct CMUnitTest tests[] = {
		cmocka_unit_test(udev_device_from_syspath_existing_succeeds),
		cmocka_unit_test(udev_device_from_syspath_nonexisting_fails),

		cmocka_unit_test(udev_device_from_nulstr_succeeds),

		cmocka_unit_test(udev_device_from_devnum_existing_succeeds),
		cmocka_unit_test(udev_device_from_devnum_nonexisting_fails),
		cmocka_unit_test(udev_device_from_devnum_with_invalid_type_fails),
		cmocka_unit_test(udev_device_from_devnum_with_wrong_type_fails),
		cmocka_unit_test(udev_device_from_devnum_with_invalid_type_fails),

		cmocka_unit_test(udev_device_get_parent_succeeds),
		cmocka_unit_test(udev_device_get_parent_fails_at_root),
		cmocka_unit_test(udev_device_get_parent_with_subsystem_devtype_succeeds),
		cmocka_unit_test(udev_device_get_parent_with_subsystem_devtype_succeeds_with_missing_devtype),
		cmocka_unit_test(udev_device_get_parent_with_subsystem_devtype_fails_with_non_matching_devtype),

		cmocka_unit_test(udev_device_get_subsystem_succeeds),
		cmocka_unit_test(udev_device_get_syspath_succeeds),
		cmocka_unit_test(udev_device_get_sysname_succeeds),
		cmocka_unit_test(udev_device_get_devnode_succeeds),
		cmocka_unit_test(udev_device_get_devtype_succeeds),
		cmocka_unit_test(udev_device_get_devnum_succeeds),
		cmocka_unit_test(udev_device_get_property_value_succeeds),
		cmocka_unit_test(udev_device_get_property_value_returns_null_for_nonexisting_property),
		cmocka_unit_test(udev_device_get_property_value_returns_null_for_prefix),
		cmocka_unit_test(udev_device_get_sysattr_value_succeeds),
		cmocka_unit_test(udev_device_get_sysattr_value_succeeds_with_multiple_values),
		cmocka_unit_test(udev_device_get_sysattr_value_succeeds_with_cached_value),
		cmocka_unit_test(udev_device_get_sysattr_value_fails_with_nonexisting_sysvalue),
	};

	if ((test_parse_opts(argc, argv)) < 0)
		return -1;

	return cmocka_run_group_tests_name("device", tests, setup, teardown);
}
