/*
 * Copyright (C) Patrick Steinhardt, 2020
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef LIBSUDEV_PRIVATE_H
#define LIBSUDEV_PRIVATE_H

#include <dirent.h>
#include <fcntl.h>
#include <fnmatch.h>
#include <limits.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/sysmacros.h>
#include <unistd.h>

#include "libudev.h"

#define UNUSED __attribute__((unused))

struct udev_list_entry *udev_list_prepend(struct udev_list_entry *tail, const char *name, const char *value);
void udev_list_free(struct udev_list_entry *list);
struct udev_list_entry *udev_list_sort(struct udev_list_entry *list, int (*cmp)(struct udev_list_entry *, struct udev_list_entry *));

const char *udev_syspath(void);
char *udev_syspathf(char *out, size_t bytes, const char *fmt, ...);

#endif
