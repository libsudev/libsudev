/*
 * Copyright (C) Patrick Steinhardt, 2020
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "libudev-private.h"

struct udev_list_entry {
	struct udev_list_entry *next;
	char *name;
	char *value;
};

struct udev_list_entry *udev_list_prepend(struct udev_list_entry *tail, const char *name, const char *value)
{
	struct udev_list_entry *entry = malloc(sizeof(*entry));
	entry->name = strdup(name);
	entry->value = value ? strdup(value) : NULL;
	entry->next = tail;
	return entry;
}

void udev_list_free(struct udev_list_entry *list)
{
	struct udev_list_entry *next;
	while (list) {
		next = list->next;
		free(list->name);
		free(list->value);
		free(list);
		list = next;
	}
}

const char *udev_list_entry_get_name(struct udev_list_entry *e)
{
	if (e)
		return e->name;
	return NULL;
}

const char *udev_list_entry_get_value(struct udev_list_entry *e)
{
	if (e)
		return e->value;
	return NULL;
}

struct udev_list_entry *udev_list_entry_get_next(struct udev_list_entry *e)
{
	if (e)
		return e->next;
	return NULL;
}

struct udev_list_entry *udev_list_entry_get_by_name(struct udev_list_entry *e, const char *name)
{
	while (e) {
		if (!strcmp(e->name, name))
			return e;
		e = e->next;
	}
	return NULL;
}

static void split(struct udev_list_entry *list, struct udev_list_entry **a, struct udev_list_entry **b)
{
	struct udev_list_entry *slow = list, *fast = list->next;

	while (fast) {
		fast = fast->next;
		if (fast) {
			slow = slow->next;
			fast = fast->next;
		}
	}

	*a = list;
	*b = slow->next;
	slow->next = NULL;
}

static struct udev_list_entry *merge(struct udev_list_entry *a, struct udev_list_entry *b,
				     int (*cmp)(struct udev_list_entry *, struct udev_list_entry *))
{
	if (!a)
		return b;
	if (!b)
		return a;

	if (cmp(a, b) <= 0) {
		a->next = merge(a->next, b, cmp);
		return a;
	} else {
		b->next = merge(b->next, a, cmp);
		return b;
	}
}

struct udev_list_entry *udev_list_sort(struct udev_list_entry *list, int (*cmp)(struct udev_list_entry *, struct udev_list_entry *))
{
	struct udev_list_entry *a, *b;
	if (!list || list->next == NULL)
		return list;

	split(list, &a, &b);
	a = udev_list_sort(a, cmp);
	b = udev_list_sort(b, cmp);

	return merge(a, b, cmp);
}
