/*
 * Copyright (C) Patrick Steinhardt, 2020
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "libudev-private.h"

struct udev_enumerate {
	unsigned refcount;
	struct udev *udev;
	struct udev_list_entry *devices;
	struct {
		struct udev_list_entry *sysname;
		struct udev_list_entry *subsystem;
	} matches;
};

struct udev_enumerate *udev_enumerate_new(struct udev *udev)
{
	struct udev_enumerate *enumerate = NULL;

	if ((enumerate = calloc(1, sizeof(*enumerate))) == NULL)
		return NULL;
	enumerate->refcount = 1;
	enumerate->udev = udev_ref(udev);

	return enumerate;
}

struct udev_enumerate *udev_enumerate_unref(struct udev_enumerate *enumerate)
{
	if (!enumerate || --enumerate->refcount != 0)
		return NULL;
	udev_unref(enumerate->udev);
	udev_list_free(enumerate->devices);
	udev_list_free(enumerate->matches.sysname);
	udev_list_free(enumerate->matches.subsystem);
	free(enumerate);
	return NULL;
}

int udev_enumerate_add_match_subsystem(struct udev_enumerate *enumerate, const char *subsystem)
{
	if (!enumerate)
		return -1;
	if (!subsystem)
		return 0;
	enumerate->matches.subsystem = udev_list_prepend(enumerate->matches.subsystem,
							 subsystem, NULL);
	return 0;
}

int udev_enumerate_add_match_sysname(struct udev_enumerate *enumerate, const char *sysname)
{
	if (!enumerate)
		return -1;
	if (!sysname)
		return 0;
	enumerate->matches.sysname = udev_list_prepend(enumerate->matches.sysname,
						       sysname, NULL);
	return 0;
}

struct udev_list_entry *udev_enumerate_get_list_entry(struct udev_enumerate *enumerate)
{
	if (enumerate)
		return enumerate->devices;
	return NULL;
}

static int enumerate_foreach_dir(struct udev_enumerate *enumerate, const char *path, int (*callback)(struct udev_enumerate *, const char *))
{
	struct dirent *dir;
	DIR *dirp;
	int err = 0;

	if ((dirp = (opendir(path))) == NULL)
		return -1;

	while ((dir = readdir(dirp)) != NULL) {
		char subdir[PATH_MAX];
		struct stat st;

		if (!strcmp(dir->d_name, ".") || !strcmp(dir->d_name, ".."))
			continue;

		snprintf(subdir, sizeof(subdir), "%s/%s", path, dir->d_name);

		if ((err = stat(subdir, &st)) < 0)
			goto out;

		if (!S_ISDIR(st.st_mode))
			continue;

		if ((err = callback(enumerate, subdir)) < 0)
			goto out;
	}

out:
	closedir(dirp);
	return err;
}

static int matches(struct udev_list_entry *haystack, const char *needle)
{
	struct udev_list_entry *entry;

	if (!haystack)
		return 1;
	if (!needle)
		return 0;

	udev_list_entry_foreach(entry, haystack) {
		if (fnmatch(udev_list_entry_get_name(entry), needle, 0) != 0)
			continue;
		return 1;
	}

	return 0;
}

static int enumerate_scan_device(struct udev_enumerate *enumerate, const char *path)
{
	struct udev_device *device;

	if ((device = udev_device_new_from_syspath(enumerate->udev, path)) == NULL)
		return -1;

	if (matches(enumerate->matches.sysname, udev_device_get_sysname(device)) &&
	    matches(enumerate->matches.subsystem, udev_device_get_subsystem(device)))
		enumerate->devices = udev_list_prepend(enumerate->devices, path, NULL);

	udev_device_unref(device);
	return 0;
}

static int enumerate_scan_devices(struct udev_enumerate *enumerate, const char *path)
{
	return enumerate_foreach_dir(enumerate, path, enumerate_scan_device);
}

static int enumerate_scan_subdevices(struct udev_enumerate *enumerate, const char *path)
{
	char subdir[PATH_MAX];
	snprintf(subdir, sizeof(subdir), "%s/devices", path);
	return enumerate_foreach_dir(enumerate, subdir, enumerate_scan_device);
}

static int device_path_cmp(struct udev_list_entry *a, struct udev_list_entry *b)
{
	return strcmp(udev_list_entry_get_name(a), udev_list_entry_get_name(b));
}

int udev_enumerate_scan_devices(struct udev_enumerate *enumerate)
{
	char path[PATH_MAX];

	udev_list_free(enumerate->devices);
	enumerate->devices = NULL;

	if (enumerate_foreach_dir(enumerate, udev_syspathf(path, sizeof(path), "/bus"), enumerate_scan_subdevices) < 0 ||
	    enumerate_foreach_dir(enumerate, udev_syspathf(path, sizeof(path), "/class"), enumerate_scan_devices) < 0)
		return -1;

	enumerate->devices = udev_list_sort(enumerate->devices, device_path_cmp);

	return 0;
}
