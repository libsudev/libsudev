/*
 * Copyright (C) Patrick Steinhardt, 2020
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "libudev-private.h"

#include <linux/netlink.h>
#include <sys/socket.h>

struct udev_monitor {
	unsigned refcount;
	struct udev *udev;
	struct sockaddr_nl addr;
	int fd;
	struct {
		struct udev_list_entry *subsystem;
		struct udev_list_entry *devtype;
	} matches;
};

struct udev_monitor *udev_monitor_new_from_netlink(struct udev *udev, const char *name)
{
	struct udev_monitor *monitor = NULL;

	if (!name || (strcmp(name, "udev") && strcmp(name, "kernel")))
		goto on_error;

	if ((monitor = calloc(1, sizeof(*monitor))) == NULL)
		goto on_error;
	monitor->refcount = 1;
	monitor->udev = udev_ref(udev);
	monitor->addr.nl_family = AF_NETLINK;
	monitor->addr.nl_groups = 0;

	if ((monitor->fd = socket(PF_NETLINK, SOCK_RAW|SOCK_CLOEXEC|SOCK_NONBLOCK, NETLINK_KOBJECT_UEVENT)) < 0)
		goto on_error;

	return monitor;

on_error:
	udev_monitor_unref(monitor);
	return NULL;
}

struct udev_monitor *udev_monitor_unref(struct udev_monitor *monitor)
{
	if (!monitor || --monitor->refcount != 0)
		return NULL;
	udev_unref(monitor->udev);
	if (monitor->fd >= 0)
		close(monitor->fd);
	udev_list_free(monitor->matches.subsystem);
	udev_list_free(monitor->matches.devtype);
	free(monitor);
	return NULL;
}

int udev_monitor_enable_receiving(struct udev_monitor *monitor)
{
	return bind(monitor->fd, (struct sockaddr *)&monitor->addr, sizeof(monitor->addr));
}

int udev_monitor_filter_add_match_subsystem_devtype(struct udev_monitor *monitor, const char *subsystem, const char *devtype)
{
	if (!monitor)
		return -1;

	if (subsystem) {
		monitor->matches.subsystem = udev_list_prepend(monitor->matches.subsystem,
							       subsystem, NULL);
	}
	if (devtype) {
		monitor->matches.devtype = udev_list_prepend(monitor->matches.devtype,
							     devtype, NULL);
	}

	return 0;
}

struct udev *udev_monitor_get_udev(struct udev_monitor *monitor)
{
	if (!monitor)
		return NULL;
	return monitor->udev;
}

int udev_monitor_get_fd(struct udev_monitor *monitor)
{
	if (!monitor)
		return -1;
	return monitor->fd;
}

static int matches(struct udev_list_entry *haystack, const char *needle)
{
	struct udev_list_entry *entry;

	if (!haystack)
		return 1;
	if (!needle)
		return 0;

	udev_list_entry_foreach(entry, haystack) {
		if (fnmatch(udev_list_entry_get_name(entry), needle, 0) != 0)
			continue;
		return 1;
	}

	return 0;
}

struct udev_device *udev_monitor_receive_device(struct udev_monitor *monitor)
{
	struct udev_device *device;
	struct msghdr msg;
	struct iovec iov;
	char buf[8192];
	ssize_t buflen, bufpos;

	iov.iov_base = &buf;
	iov.iov_len = sizeof(buf);
	memset(&msg, 0, sizeof(msg));
	msg.msg_iov = &iov;
	msg.msg_iovlen = 1;

	if ((buflen = recvmsg(monitor->fd, &msg, 0)) < 32 || (msg.msg_flags & MSG_TRUNC))
		return NULL;

	bufpos = strlen(buf) + 1;
	if ((size_t) bufpos < sizeof("a@/d") || bufpos >= buflen)
		return NULL;

	if (strstr(buf, "@/") == NULL)
		return NULL;

	device = udev_device_new_from_nulstr(monitor->udev, &buf[bufpos], buflen - bufpos);

	if (!matches(monitor->matches.subsystem, udev_device_get_subsystem(device)) &&
	    !matches(monitor->matches.devtype, udev_device_get_devtype(device)))
		return NULL;

	return device;
}
