/*
 * Copyright (C) Patrick Steinhardt, 2020
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "libudev-private.h"

struct udev_device {
	unsigned refcount;
	struct udev *udev;
	struct udev_list_entry *properties;
	struct udev_list_entry *sysattrs;
	char *devnode;
	char *subsystem;
	char *syspath;
};

static int udev_device_read_properties(struct udev_device *dev)
{
	struct udev_list_entry *properties = NULL;
	char uevent[PATH_MAX], *line = NULL;
	size_t linelen = 0;
	FILE *f;

	snprintf(uevent, sizeof(uevent), "%s/uevent", dev->syspath);
	if ((f = fopen(uevent, "r")) == NULL)
		return -1;

	while ((getline(&line, &linelen, f)) > 0) {
		char *value;

		if ((value = strchr(line, '=')) == NULL)
			continue;

		*value = '\0';
		value++;
		if (value[strlen(value) - 1] == '\n')
			value[strlen(value) - 1] = '\0';

		properties = udev_list_prepend(properties, line, value);
	}

	dev->properties = properties;

	free(line);
	fclose(f);
	return 0;
}

struct udev_device *udev_device_new_from_devnum(struct udev *udev, char type, dev_t devnum)
{
	struct udev_device *dev;
	const char *typestr;
	char path[PATH_MAX];

	switch (type) {
		case 'b':
			typestr = "block";
			break;
		case 'c':
			typestr = "char";
			break;
		default:
			return NULL;
	}

	if ((udev_syspathf(path, sizeof(path), "/dev/%s/%u:%u", typestr, major(devnum), minor(devnum))) == NULL)
		return NULL;

	dev = udev_device_new_from_syspath(udev, path);

	return dev;
}

static int resolve_symlink(char *out, size_t bytes, const char *symlink)
{
	char target[PATH_MAX], *t = target;
	ssize_t target_length;

	strncpy(out, symlink, bytes);

	if ((target_length = readlink(symlink, target, sizeof(target))) < 0)
		return -1;
	target[target_length] = '\0';

	if (strlen(symlink) + target_length > bytes)
		return -1;

	while (1) {
		char *slash = strrchr(out, '/');
		if (!slash)
			return -1;
		*slash = '\0';

		if (strncmp(t, "../", 3))
			break;
		t += 3;
	}

	snprintf(out + strlen(out), bytes - strlen(out), "/%s", t);

	return 0;
}

static struct udev_device *udev_device_new(struct udev *udev)
{
	struct udev_device *dev;

	if ((dev = calloc(1, sizeof(*dev))) == NULL)
		return NULL;
	dev->refcount = 1;
	dev->udev = udev_ref(udev);

	return dev;
}

struct udev_device *udev_device_new_from_syspath(struct udev *udev, const char *syspath)
{
	char path[PATH_MAX];
	struct udev_device *dev = NULL;
	struct stat st;

	if (lstat(syspath, &st) != 0)
		goto on_error;

	if (S_ISLNK(st.st_mode)) {
		if (resolve_symlink(path, sizeof(path), syspath) < 0 || stat(path, &st) != 0)
			goto on_error;
	} else {
		strcpy(path, syspath);
	}

	if (!S_ISDIR(st.st_mode))
		goto on_error;

	if ((dev = udev_device_new(udev)) == NULL ||
	    (dev->syspath = strdup(path)) == NULL ||
	    (udev_device_read_properties(dev)) < 0)
		goto on_error;

	return dev;

on_error:
	udev_device_unref(dev);
	return NULL;
}

struct udev_device *udev_device_new_from_nulstr(struct udev *udev, char *buf, ssize_t buflen)
{
	struct udev_device *dev = NULL;
	const char *devpath = NULL;
	char syspath[PATH_MAX];

	while (buf < (buf + buflen)) {
		if (!strncmp(buf, "DEVPATH=", strlen("DEVPATH="))) {
			devpath = buf + strlen("DEVPATH=");
			break;
		}
		buf += strlen(buf) + 1;
	}
	if (!devpath)
		goto on_error;

	if ((udev_syspathf(syspath, sizeof(syspath), "/%s", devpath)) == NULL ||
	    (dev = udev_device_new_from_syspath(udev, syspath)) == NULL)
		goto on_error;

	return dev;

on_error:
	udev_device_unref(dev);
	return NULL;
}

struct udev_device *udev_device_ref(struct udev_device *dev)
{
	if (!dev)
		return NULL;
	dev->refcount++;
	return dev;
}

struct udev_device *udev_device_unref(struct udev_device *dev)
{
	if (!dev || --dev->refcount != 0)
		return NULL;
	udev_unref(dev->udev);
	udev_list_free(dev->properties);
	udev_list_free(dev->sysattrs);
	free(dev->devnode);
	free(dev->subsystem);
	free(dev->syspath);
	free(dev);
	return NULL;
}

struct udev *udev_device_get_udev(struct udev_device *dev)
{
	if (dev)
		return dev->udev;
	return NULL;
}

const char *udev_device_get_action(struct udev_device *dev UNUSED)
{
	return NULL;
}

const char *udev_device_get_devnode(struct udev_device *dev)
{
	const char *property;
	char devnode[4096];

	if (!dev)
		return NULL;
	if (dev->devnode)
		return dev->devnode;

	if ((property = udev_device_get_property_value(dev, "DEVNAME")) == NULL)
		return NULL;
	snprintf(devnode, sizeof(devnode), "/dev/%s", property);

	return dev->devnode = strdup(devnode);
}

const char *udev_device_get_devtype(struct udev_device *dev)
{
	return udev_device_get_property_value(dev, "DEVTYPE");
}

dev_t udev_device_get_devnum(struct udev_device *dev)
{
	const char *major, *minor;

	if  (!dev)
		return makedev(0, 0);

	if ((major = udev_device_get_property_value(dev, "MAJOR")) == NULL ||
	    (minor = udev_device_get_property_value(dev, "MINOR")) == NULL)
		return makedev(0, 0);

	return makedev(strtoull(major, NULL, 10), strtoull(minor, NULL, 10));
}

int udev_device_get_is_initialized(struct udev_device *dev UNUSED)
{
	return 1;
}

struct udev_device *udev_device_get_parent(struct udev_device *dev)
{
	char path[PATH_MAX];
	const char *subdir;

	snprintf(path, sizeof(path), "%s", dev->syspath);
	subdir = path + strlen(udev_syspath());
	while (1) {
		struct udev_device *parent = NULL;
		char *pos;

		pos = strrchr(subdir, '/');
		if (pos == NULL || pos < &subdir[2])
			break;
		pos[0] = '\0';
		parent = udev_device_new_from_syspath(dev->udev, path);
		if (parent != NULL)
			return parent;
	}

	return NULL;
}

struct udev_device *udev_device_get_parent_with_subsystem_devtype(struct udev_device *dev, const char *subsystem, const char *devtype)
{
	struct udev_device *parent = udev_device_get_parent(dev), *next;

	while (parent) {
		const char *parent_subsystem = udev_device_get_subsystem(parent);
		if (parent_subsystem && !strcmp(parent_subsystem, subsystem)) {
			const char *parent_devtype = udev_device_get_devtype(parent);

			if (!devtype || !strcmp(devtype, parent_devtype))
				return parent;
		}

		next = udev_device_get_parent(parent);
		udev_device_unref(parent);
		parent = next;
	}

	return NULL;
}

struct udev_list_entry *udev_device_get_properties_list_entry(struct udev_device *dev)
{
	if (dev)
		return dev->properties;
	return NULL;
}

const char *udev_device_get_property_value(struct udev_device *dev, const char *property)
{
	struct udev_list_entry *e;
	if (!dev)
		return NULL;
	if ((e = udev_list_entry_get_by_name(dev->properties, property)) == NULL)
		return NULL;
	return udev_list_entry_get_value(e);
}

const char *udev_device_get_subsystem(struct udev_device *dev)
{
	char path[PATH_MAX], link[PATH_MAX], *slash;
	ssize_t bytes;

	if (!dev)
		return NULL;
	if (dev->subsystem)
		return dev->subsystem;

	snprintf(path, sizeof(path), "%s/subsystem", dev->syspath);
	if ((bytes = readlink(path, link, sizeof(link))) < 0)
		return NULL;
	if ((size_t) bytes >= sizeof(link))
		return NULL;
	link[bytes] = '\0';

	slash = strrchr(link, '/');
	if (!slash)
		return NULL;

	return dev->subsystem = strdup(slash + 1);
}

const char *udev_device_get_sysname(struct udev_device *dev)
{
	char *slash;

	if (!dev)
		return NULL;

	slash = strrchr(dev->syspath, '/');
	if (!slash)
		return NULL;

	return slash + 1;
}

const char *udev_device_get_syspath(struct udev_device *dev)
{
	if (dev)
		return dev->syspath;
	return NULL;
}

const char *udev_device_get_sysattr_value(struct udev_device *dev, const char *sysattr)
{
	char path[PATH_MAX], *contents = NULL;
	const char *value = NULL;
	struct udev_list_entry *e = NULL;
	off_t bytes = 0;
	struct stat st;
	int fd = -1;

	if (!dev)
		goto out;

	if ((e = udev_list_entry_get_by_name(dev->sysattrs, sysattr)) != NULL)
		return udev_list_entry_get_value(e);

	snprintf(path, sizeof(path), "%s/%s", dev->syspath, sysattr);

	if (stat(path, &st) < 0)
		goto out;

	if ((contents = malloc(st.st_size + 1)) == NULL)
		goto out;

	if ((fd = open(path, O_RDONLY)) < 0)
		goto out;

	while (bytes < st.st_size) {
		ssize_t bytes_read = read(fd, contents + bytes, st.st_size - bytes);
		if (bytes_read <= 0)
			return NULL;
		bytes += bytes_read;
	}
	contents[bytes] = '\0';

	if ((dev->sysattrs = udev_list_prepend(dev->sysattrs, sysattr, contents)) == NULL)
		goto out;
	value = udev_list_entry_get_value(dev->sysattrs);

out:
	if (fd >= 0)
		close(fd);
	free(contents);
	return value;
}
