/*
 * Copyright (C) Patrick Steinhardt, 2020
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "libudev-private.h"

struct udev {
	unsigned refcount;
};

struct udev *udev_new(void)
{
	struct udev *udev;
	if ((udev = calloc(1, sizeof(*udev))) == NULL)
		return NULL;
	udev->refcount = 1;
	return udev;
}

struct udev *udev_ref(struct udev *udev)
{
	if (!udev)
		return NULL;
	udev->refcount++;
	return udev;
}

struct udev *udev_unref(struct udev *udev)
{
	if (!udev || --udev->refcount != 0)
		return NULL;
	free(udev);
	return NULL;
}

const char *udev_syspath(void)
{
	static const char *prefix;
	if (!prefix) {
		prefix = getenv("UDEV_SYSPATH_PREFIX");
		if (!prefix)
			prefix = "/sys";
	}
	return prefix;
}

char *udev_syspathf(char *out, size_t bytes, const char *fmt, ...)
{
	va_list ap;
	int len;

	if ((len = snprintf(out, bytes, "%s", udev_syspath())) < 0)
		return NULL;

	va_start(ap, fmt);
	if ((len = vsnprintf(out + len, bytes - len, fmt, ap)) < 0)
		return NULL;
	va_end(ap);

	return out;
}
